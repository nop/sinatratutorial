class App < Sinatra::Base
  configure do
    set :server, :puma
    set :show_exceptions, false
    set :bind, "0.0.0.0"
    set :port, ENV["PORT"] || "8080"
  end

  configure :development do
    set :logging, Logger::DEBUG
  end

  get "/" do
    name = ENV["NAME"] || "World"
    "Hello #{name}!"
  end
end
