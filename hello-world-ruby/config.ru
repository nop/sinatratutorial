require 'bundler/setup'
$stdout.sync = true

require 'logger'
require 'sinatra/base'
require './app'

run Rack::URLMap.new(
  '/' => App,
)
