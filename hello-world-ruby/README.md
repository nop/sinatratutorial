# building & deploying

gcloud builds submit --tag gcr.io/marcchung/helloworld-ruby

gcloud run deploy --image gcr.io/marcchung/helloworld-ruby --platform managed

# developing locally

docker build -t gcr.io/marcchung/helloworld-ruby .

docker run -it --rm -p 3000:4567 gcr.io/marcchung/helloworld-ruby
# ssh in to the container

docker run -it --rm --entrypoint /bin/sh -p 3000:4567 gcr.io/marcchung/helloworld-ruby

# Google Run notes

https://cloud.google.com/run/docs/reference/container-contract

# App

https://helloworld-ruby-cq3pksjtta-uc.a.run.app/